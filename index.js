const fs = require('fs')
const https = require('https')
const path = require('path')
const ffmpeg = require('fluent-ffmpeg')
const express = require('express')
const app = express()
const animeRoot = '/anime'
const port = 8117
const minStartDelay = 15 // start no less than 15 seconds from the beginning

const privateKey = fs.readFileSync('/etc/letsencrypt/live/animeops.giddeongarber.info/privkey.pem', 'utf8')
const certificate = fs.readFileSync('/etc/letsencrypt/live/animeops.giddeongarber.info/cert.pem', 'utf8')
const ca = fs.readFileSync('/etc/letsencrypt/live/animeops.giddeongarber.info/chain.pem', 'utf8')

const credentials = { key: privateKey, cert: certificate, ca: ca }

// host the clips dynamically using ffmpeg to trim the file down
app.get('/clip/:len/:video.webm', (req, res) => {
  const clipLen = +req.params.len
  const path = `${animeRoot}/${req.params.video}.webm`
  if (!fs.existsSync(path)) {
    res.status(404).end('Not found')
    return
  }

  ffmpeg.ffprobe(path, (err, metadata) => {
    if (err) {
      res.status(500).end(err)
    } else {
      const duration = metadata.format.duration
      const earliest = minStartDelay
      const latest = duration - clipLen
      const start = earliest + (Math.random() * (latest - earliest))
      console.log(`Serving '${path}' from ${start}s (${clipLen}s)`)
      res.contentType('webm')
      ffmpeg(path)
        .seekInput(start)
        .duration(clipLen)
        .audioCodec('copy')
        .videoCodec('copy')
        .toFormat('webm')
        .on('error', (err) => console.log('An error occurred: ' + err.message))
        .pipe(res, { end: true })
    }
  })
})

// host the full clip as a standard static file
app.use('/full', express.static(animeRoot))

// host a special endpoint to get a random filename
app.get('/random', (req, res) => {
  const files = fs.readdirSync(animeRoot)
  const file = files[Math.floor(Math.random() * files.length)]
  res.end(path.basename(file, path.extname(file)))
})

// host a special endpoint to get the number of files
app.get('/count', (req, res) => {
  res.end(`${fs.readdirSync(animeRoot).length}`)
})

// host a special dynamic page that can be used to test how well each file streams
app.get('/test', (req, res) => {
  res.type('html')

  const files = fs.readdirSync(animeRoot)
  const filesWithModDate = files.map((x) => { return { file: x, modDate: fs.statSync(path.join(animeRoot, x)).mtime }})
  const html = []

  html.push('<html>')
  html.push('<head>')
  html.push('<title>Omnishots Anime Clip Test</title>')
  html.push('</head>')
  html.push('<body>')

  filesWithModDate.forEach(f => {
    html.push(`[${f.modDate.toDateString()}] <a href="/clip/6/${f.file}">${f.file}</a><br>`)
  })

  html.push('</body>')
  html.push('</html>')

  res.end(html.join(''))
})

var httpsServer = https.createServer(credentials, app)
httpsServer.listen(port)
