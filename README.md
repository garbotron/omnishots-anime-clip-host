Simple server written in NodeJS to serve anime clips under the /anime directory, for use with [Omnishots](https://gitlab.com/garbotron/omnishots).

Notes on video encoding: this server assumes all clips are encoded in WebM format.
The encoding settings should be optimized for seeking, which means a lot of keyframes.

Recommended ffmpeg settings: `ffmpeg -i (original file) -c:v libvpx-vp9 -crf 32 -b:v 0 -g 30 -c:a libvorbis (output file)`
